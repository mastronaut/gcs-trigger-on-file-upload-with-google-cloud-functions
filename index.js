/**
 * Background Cloud Function to be triggered by Cloud Storage
 * when a new file is uploaded by users in a GCS bucket.
 *
 * @param {object} file The Cloud Storage file metadata.
 * @param {object} context The event metadata.
 */
exports.triggerOnGCSUpload = async (file, context) => {
    const os = require('os');
    const bucketName = 'user_uploads';

    // Collect GCS file upload metadata
    const creationTimestamp = file.timeCreated;
    const filename = file.name;
    const mediaLink = file.mediaLink;
    const destFilePath = os.tmpdir() + '/img.jpeg';

    // Download file locally
    await downloadUploadedFile(bucketName, filename, destFilePath);

    // Run inference against GCS object
    const inference = await requestInference(destFilePath);

    console.log('inference: ', inference);
  
    // Store inference in BigQuery
    await insertInferenceInBigQuery(mediaLink, creationTimestamp, inference.class, inference.score);
};

function requestInference(filePath) {
    console.log('Running inference against image...');

    const projectId = 'my-project-id';
    const location = 'gcp-region'; // eg. us-central1
    const modelId = 'XXXX' ; // can be find in AutoML, on the model page

    // Imports the Google Cloud AutoML library
    const {PredictionServiceClient} = require('@google-cloud/automl').v1;
    const fs = require('fs');

    // Instantiates a client
    const client = new PredictionServiceClient();

    // Read the file content for translation.
    const content = fs.readFileSync(filePath);

    async function predict() {
        // Construct request
        const request = {
            name: client.modelPath(projectId, location, modelId),
            payload: {
                image: {
                    imageBytes: content,
                },
            },
        };

        const [response] = await client.predict(request);

        let inference = null;

        for (const annotationPayload of response.payload) {
            inference = {
                class: annotationPayload.displayName,
                score: annotationPayload.classification.score
            };
        }

        return inference;
    }

    return predict();
}

function downloadUploadedFile(bucketName, filename, destFilePath) {
    console.log('Downloading file uploaded in GCS...');

    // Imports the Google Cloud client library
    const {Storage} = require('@google-cloud/storage');

    // Creates a client
    const storage = new Storage();

    async function downloadFile() {
        const options = {
            destination: destFilePath,
        };

        // Downloads the file
        await storage.bucket(bucketName).file(filename).download(options);

        console.log(`gs://${bucketName}/${filename} downloaded to ${destFilePath}.`);
    }

    return downloadFile();
}

function insertInferenceInBigQuery(image_url, upload_timestamp, inference_class, inference_score) {
    console.log('Pushing the inference result in BigQuery...')
    const {BigQuery} = require('@google-cloud/bigquery');
    const bigquery = new BigQuery();
    const dataset = bigquery.dataset('my-dataset');
    const table = dataset.table('my-table');

    return table.insert({
        image_url,
        upload_timestamp,
        inference_class,
        inference_score
    }, (err) => console.log('err: ', err));
}